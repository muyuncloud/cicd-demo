require "webrick"

server = WEBrick::HTTPServer.new :port = 5000

server.mount_proc '/' do |request response|
    response.body = 'hello world!'
end

server start

